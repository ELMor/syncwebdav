
package org.elm.box;

import java.util.Vector;

import org.elm.box.vfs.FSDisk;
import org.elm.box.vfs.FSWebDAV;
import org.elm.box.vfs.Node;


public class Main {
	public static void main(String args[]){		
		try {
			
			System.setProperty("log.level", "DEBUG");
			
			FSWebDAV fsRem=new FSWebDAV("https://www.box.net/dav/tmp","eladio.linares@gmail.com","rpq231wg");
			FSDisk fsLoc=new FSDisk("d:\\tmp");

			Node local=fsLoc.getRoot();
			Node remot=fsRem.getRoot(); 			
			Vector<Node> diff=local.calculateGap(remot);
			fsLoc.synchWith(fsRem,diff);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
